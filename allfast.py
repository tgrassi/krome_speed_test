from subprocess import call
from subprocess import Popen, PIPE
import os,glob,shutil,time

test_iterations = 10
do_compile = True
file_output = "speed_test.log"

total_time_start = time.time()
testpath = "tests/" #where the tests are located
tests = sorted([x[0].replace(testpath,"") for x in os.walk(testpath) if x[0]!=testpath])

#tests = [x for x in tests if x!="cloud"]
execution_times = {x:[] for x in tests}

for test in tests:
	if not do_compile:
		break
	print
	print "#########################################################################################################"
	print "                                          compiling test: "+test
	print "#########################################################################################################"
	print

	#clear directory
	for ff in glob.glob("build/*"):
		os.remove(ff)

	#call krome
	callarg = ["./krome", "-test="+test,"-unsafe"]
	print callarg
	call(callarg)

	#move to build directory
	os.chdir("build/")

	#make clean
	call(["make","clean"])

	#compile full debug
	call(["make"])

	#move to KROME directory
	os.chdir("..")

	#copy build folder to build_[test] folder
	call(["cp","-r","build","build_"+test])


# do test several times to have a decent time statistics
for i in range(test_iterations):
	# loop on tests to execute them
	for test in tests:

		print
		print "#########################################################################################################"
		print "                                  running test: "+test+" "+str(i+1)+"/"+str(test_iterations)
		print "#########################################################################################################"
		print

		#move to build directory
		os.chdir("build_"+test+"/")

		#store starting time
		time_start = time.time()

		#run executable
		call(["./test"])

		#store execution time
		execution_times[test].append(time.time() - time_start)

		#move to KROME directory
		os.chdir("..")


def table_row(arg_list, col_width=20):
	row = [str(x)+(" "*(col_width-len(str(x)))) for x in arg_list]
	return "".join(row)

fout = open(file_output, "w")
# print results to screen
fout.write( table_row(["test"]+["#"+str(i+1) for i in range(test_iterations)]+[" average"])+"\n")
for test, dtime in execution_times.iteritems():
	average = sum(dtime)/len(dtime)
	fout.write(table_row([test]+[average]+dtime)+"\n")
fout.close()
print "output written in "+file_output
print "total time (min):", (time.time() - total_time_start)/60.
print "DONE!"


