## KROME speed test
This is a version of [KROME](https://bitbucket.org/tgrassi/krome/overview) to test the speed of the code on your machine.     
KROME changest: ae6856c9161cb7611fc98fce52a6fa1949319890

Clone
```
git clone https://tgrassi@bitbucket.org/tgrassi/krome_speed_test.git
```

and type
```
cd krome_speed_test
python allfast.py
```
Wait.    

This will take a series of tests. The total time could vary depend on your machine (15-60 min).      
It is better to avoid any other operation on your machine when you are testing (time for a sandwich).       
Results are stored in `speed_test.log`.

Here some results from     
CPU: Intel(R) Core(TM) i7-7820X CPU @ 3.60GHz.   
COMPILER: ifort (IFORT) 18.0.1 20171018        

(first and last column of `speed_test.log`)

|name|average|
|----|----|
|map | 2.76652269363 |
|collapseCO | 5.12260375023 |
|shock1D | 9.82546122074 |
|collapseZ | 4.78340778351 |
|shock1Dcool | 31.3931821346 |
|cloud | 15.3363665819 |

More results on the [wiki](https://bitbucket.org/tgrassi/krome_speed_test/wiki/Home).